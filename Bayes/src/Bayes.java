import java.util.Set;
import java.util.Map.Entry;
import  java.util.Hashtable;

public class Bayes {

    public static int maxofarray(double []array)
    {
        int index = -1;
        double value = -1000;
        for(int i = 0; i < array.length; i++)
        {
            if(value < array[i])
            {
                value = array[i];
                index = i;
            }
        }
        return index;
    }

    public static void main(String[] args) {
        int training[][] = {{1, 1, -1}, {1, 2, -1}, {1, 2, 1}, {1, 1, 1}, {1, 1, -1}, {2, 1, -1}, {2, 2, -1}, {2, 2, 1}, {2, 3, 1}, {2, 3, 1}, {3, 3, 1}, {3, 2, 1}, {3, 2, 1}, {3, 3, 1}, {3, 3, -1}};
        int test[] = {3,3};
        Hashtable<Integer, Integer> c_collection = new Hashtable<Integer, Integer>();

        for (int i = 0; i < training.length; i++) {
            if (!c_collection.containsKey(training[i][training[i].length-1])) {
                c_collection.put(training[i][training[i].length-1], 1);
            } else {
                c_collection.put(training[i][training[i].length-1], c_collection.get(training[i][training[i].length-1]) + 1);
            }
        }
        Set<Entry<Integer, Integer>> hs = c_collection.entrySet();
        int[] c_array = new int[c_collection.size()];
        double[] c_pro = new double[c_collection.size()];
        int m = 0;
        for (Entry<Integer, Integer> e:hs)
        {
            c_array[m] = e.getKey();
            double temp = e.getValue();
            c_pro[m] =  temp/training.length;
            m++;
        }

        double[] x_exponent =  new double[c_array.length];
        for(int k = 0; k < c_array.length; k++)
        {

            for(int j = 0; j < test.length; j++)
            {
                double temp = 0;
                for(int i = 0; i < training.length; i++)
                {
                    if(training[i][training[i].length - 1] == c_array[k] && training[i][j] == test[j])
                    {
                        temp ++;
                    }
                }
                temp = temp/(c_pro[k] * training.length);    //smoothing
                if(x_exponent[k] == 0)
                {
                    x_exponent[k] += temp;
                }
                else
                {
                    x_exponent[k] *= temp;
                }
            }
            x_exponent[k] *= c_pro[k];
        }
        System.out.print("the most probable class is " + c_array[maxofarray(x_exponent)] +", the joint probability is " + x_exponent[maxofarray(x_exponent)]);

    }
}
